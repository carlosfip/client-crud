package br.com.gupy.clientcrud.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.hibernate.validator.constraints.br.CPF;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.time.LocalDate;


@Entity
@Table(name = "T_CLIENTE")
public class Cliente {
    @Id
    @Column(name="ID")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGeneratorCliente")
    @SequenceGenerator(name = "sequenceGeneratorCliente", sequenceName = "S_CLIENTE", allocationSize=1)
    private Long id;

    @Column(name="NOME")
    @NotBlank(message = "Campo obrigatorio")
    private String nome;

    @Column(name="CPF")
    @CPF(message = "CPF Invalido")
    private String cpf;

    @JsonFormat(pattern="dd-MM-yyyy")
    @Column(name="DATANASCIMENTO")
    private LocalDate dataNascimento;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public LocalDate getDataNascimento() {
        return dataNascimento;
    }

    public void setDataNascimento(LocalDate dataNascimento) {
        this.dataNascimento = dataNascimento;
    }
}
