package br.com.gupy.clientcrud.resource;

import br.com.gupy.clientcrud.domain.Cliente;
import br.com.gupy.clientcrud.dto.ClienteDTO;
import br.com.gupy.clientcrud.resource.utils.QueryBuscaCliente;
import br.com.gupy.clientcrud.service.ClienteService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
public class ClienteResource {

    private final ClienteService clienteService;

    public ClienteResource(ClienteService clienteService) {
        this.clienteService = clienteService;
    }

    @GetMapping("/clientes")
    public ResponseEntity<Page<ClienteDTO>> buscarCliente(QueryBuscaCliente criteria, Pageable pageable) {
        return ResponseEntity.ok(clienteService.buscar(criteria, pageable));
    }

    @PostMapping("/clientes")
    public ResponseEntity<Cliente> post(@RequestBody @Valid Cliente cliente) throws Exception {
        QueryBuscaCliente query = new QueryBuscaCliente();
        query.setCpf(cliente.getCpf());
        ResponseEntity<Page<ClienteDTO>> pageResponseEntity = buscarCliente(query, PageRequest.of(0, 1));
        if (!pageResponseEntity.getBody().getContent().isEmpty()) {
            throw new Exception("Cliente já cadastrado com o cpf " + cliente.getCpf());
        }
        return ResponseEntity.ok(clienteService.salvarCliente(cliente));
    }

    @PutMapping("/clientes")
    public ResponseEntity<Cliente> put(@RequestBody @Valid Cliente cliente) throws Exception {
        if (cliente.getId() == null) {
            throw new Exception("Não foi possivel encontrar o cliente - (id está null)");
        }
        return ResponseEntity.ok(clienteService.salvarCliente(cliente));
    }

    @PatchMapping("/clientes/{id}")
    public ResponseEntity<List<Cliente>> atualizarNome(@PathVariable("id") Long id, @RequestBody String nome) {
        Optional<Cliente> clienteOptional = clienteService.get(id);
        return clienteOptional.map(cliente -> {
            cliente.setNome(nome);
            return ResponseEntity.ok().body(clienteService.salvarCliente(cliente));
        }).orElse(new ResponseEntity(HttpStatus.NOT_FOUND));
    }

    @DeleteMapping("/clientes/{id}")
    public @ResponseBody
    ResponseEntity<Void> delete(@PathVariable Long id) {
        boolean delete = clienteService.delete(id);
        if (delete) {
            return ResponseEntity.ok().build();
        } else {
            return ResponseEntity.noContent().build();
        }
    }
}
