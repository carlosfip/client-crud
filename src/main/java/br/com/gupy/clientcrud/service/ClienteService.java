package br.com.gupy.clientcrud.service;

import br.com.gupy.clientcrud.domain.Cliente;
import br.com.gupy.clientcrud.dto.ClienteDTO;
import br.com.gupy.clientcrud.repository.ClienteRepository;
import br.com.gupy.clientcrud.resource.utils.QueryBuscaCliente;
import org.apache.logging.log4j.util.Strings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.Optional;

@Service
public class ClienteService {

    Logger logger = LoggerFactory.getLogger(ClienteService.class);
    private final ClienteRepository clienteRepository;

    public ClienteService(ClienteRepository clienteRepository) {
        this.clienteRepository = clienteRepository;
    }

    public Optional<Cliente> get(Long id) {
        return clienteRepository.findById(id);
    }

    public Cliente salvarCliente(Cliente cliente) {
        return clienteRepository.save(cliente);
    }

    @Transactional(readOnly = true)
    public Page<ClienteDTO> buscar(QueryBuscaCliente criteria, Pageable page) {
        logger.debug("find by criteria : {}, page: {}", criteria, page);
        Cliente cliente = new Cliente();
        if (Strings.isNotBlank(criteria.getCpf())) {
            cliente.setCpf(criteria.getCpf());
        }
        if (Strings.isNotBlank(criteria.getNome())) {
            cliente.setNome(criteria.getNome());
        }

        Page<Cliente> all = clienteRepository.findAll(Example.of(cliente), page);
        if (!all.isEmpty()) {
            return all.map(c -> {
                ClienteDTO dto = new ClienteDTO();
                dto.setCpf(c.getCpf());
                dto.setNome(c.getNome());
                dto.setIdade((int) ChronoUnit.YEARS.between(c.getDataNascimento(),LocalDate.now()));
                return dto;
            });
        }
        return Page.empty();
    }

    public boolean delete(Long id) {
        Optional<Cliente> clienteOptional = clienteRepository.findById(id);
        if (clienteOptional.isPresent()) {
            clienteRepository.delete(clienteOptional.get());
            return true;
        }
        return false;
    }
}

