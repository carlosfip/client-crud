package br.com.gupy.clientcrud.repository;

import br.com.gupy.clientcrud.domain.Cliente;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ClienteRepository extends JpaRepository<Cliente, Long> {
}
